# backup
This project contains simple but effective backup solutions based on rsync via ssh.
If you use zsh you may include the scripts in your .zshrc

## Backup with WOL (backup_wol)
My backup server is running openmediavault (OMV) and is configured for Wake-On-LAN.

### Before usage
You have to make the Backup Script executable before usage.
Move .rsync_include.txt and backup to your home directory before usage.

Add ssh public key to remote machine to access it without password authentication

Set Variables in backup to
- MACSERVER: Mac-Address of your Server
- LOCALUSER: Your local Username
- IPSERVER: IP-Address of the Server
- FOLDER: Backup Destination

Add all the folders that shall be saved into the .rsync_include.txt

### How does it work?
- The bash script wakes the server
- Waits for it to be functional
- Backs up all the files from .rsync_include.txt
- Shuts the Server down to lower energy cost

## Backup with constantly running server (backup)
If you have a backup server that is running constantly you won't need the WOL features.
Therefor the backup script gets really simple.

My constantly running server is a Odroid HC2 and due to the low power consumption it runs constantly.
It is also running OMV and isaccessible via ssh in the network. 

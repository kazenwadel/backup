#!/usr/bin/env bash

printf "%s" "Checking if the server is online ..."
while ! timeout 0.2 ping -c 1 -n <IPSERVER> &> /dev/null
  do
      printf "%c" "."
done
printf "\n%s\n"  "Server is online"
rsync -r -a -v -e ssh --delete --files-from=.rsync_include.txt /home/<LOCALUSER>/ <SERVERUSER>@<IPSERVER>:/../sharedfolders/<FOLDER>

printf "%s" "Backup finished...";


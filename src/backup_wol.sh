!/usr/bin/env bash

sudo wakeonlan <MACSERVER>
printf "%s" "waiting for Server to get online ..."
while ! timeout 0.2 ping -c 1 -n <IPSERVER> &> /dev/null
  do
      printf "%c" "."
done
printf "\n%s\n"  "Server is back online"
rsync -r -a -v -e ssh --delete --files-from=.rsync_include.txt /home/<LOCALUSER>/ <SERVERUSER>@<IPSERVER>:/../sharedfolders/<FOLDER>

printf "%s" "Backup finished, Shutdown Server? <y/N>";

read -r response
case "$response" in
    [yY][eE][sS]|[yY])
  printf "%s" "Shutdown ..."
  ssh -t root@<IPSERVER> sudo poweroff
  ;;
    *)
  printf "%s" "Exiting ..."
  ;;
esac
